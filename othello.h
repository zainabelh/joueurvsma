#include <stdio.h>
#include <string.h>

/* Largeur de la grille */
#define G 8

/* Pions du jeu */
#define VIDE ' '
#define NOIR 'N'  /* joueur 1 */
#define BLANC 'B' /* joueur 2 */


/* Type du plateau de jeu */
typedef char grille[G][G];
/*prototypes des fonctions */
void init_grille (grille m);
void afficher_grille (grille m);
int case_existe (int ligne, int colonne);
void jouer_coup (grille m, int ligne, int colonne, int joueur);
void choisir_coup (grille m, int *ligne, int *colonne, int joueur);
int partie_terminee (grille m);
int peut_jouer (grille m, int joueur);
int joueur_suivant (int joueur);
void score(grille m);
coup_possible* Cree_coup(int l,int c);
liste_coups* Creeliste();
int EstVide(liste_coups* l);
void ajouter(liste_coups* l,int li,int c);
int coups(grille m,liste_coups* l,int joueur);
void copier(char src[G][G],char dest[G][G]);
int evaluation(grille m,int joueur);
void minimax(grille m,int profondeur,int joueur,int j,coup_possible* coup,int* score);
void niveau_facile(grille m,int joueur,coup_possible* co);
void jvsmchine(int niveau_difficulte,int nouv_partie);
int suivant(int joueur,char* j1,char* j2,char* j);



