#include "othello.h"
/* Fonction pour initialiser la grille */
void init_grille (grille m) {
    int i, j;

    /* On met toutes les cases de la grille a vide */
    for (i=0;i<G; i++){
        for (j=0; j<G; j++){
            m[i][j] = VIDE;
		}
	}
    /* On place les 4 premiers pions au milieu de la grille */
    m[G/2-1][G/2-1] = BLANC;    /* on place le premier pion blanc dans la case 4d */
    m[G/2-1][G/2] = NOIR;        /* on place le premier pion noir dans la case 4e */
    m[G/2][G/2-1] = NOIR;       /* on place le premier pion noir dans la case 5d */
    m[G/2][G/2] = BLANC;        /* on place le premier pion blanc dans la case 5e */
}
/* Fonction pour afficher la grille */
void afficher_grille (grille m) {
    int i, j;
    char a = 'A';

    /* Affichage des lettres des cases verticales de la grille*/
    printf ("\n    ");
    for (i=0; i<G; i++) {
        printf ("%c   " , a);
        a++;
    }

    /* Affichage de la grille */
    printf ("\n  .");
    for (i=0; i<G; i++)
        printf ("***."); /* on utilise les * pour faire le cadre de chaque case */
    printf ("\n");
    for (i=0; i<G; i++) {
		printf (" %d|", i+1); /* on place le numero de la case horizontale */
        for (j=0; j<G; j++)
            if (m[i][j] == BLANC)
                printf (" %c |", m[i][j]);
            else
                printf (" %c |", m[i][j]);
                
        printf (" \n  .");
        for (j=0; j<G; j++)
            printf ("***.");
        printf ("\n");
    }
 
}

/* Fonction pour verifier qu'une case existe */
int case_existe (int ligne, int colonne) {
	if((colonne >= 0) && (colonne < G) && (ligne >= 0) && (ligne < G)){
		return 1;
	}else{
		return 0;
	}
}
/* Fonction pour verifier qu'un coup est valide */
int coup_valide (grille m, int ligne , int colonne, int joueur) {
    int i, j, valide;
    char case_joueur, case_adversaire;

    /* On definit la couleur du joueur et celle de l'adversaire */
    /* Le premier joueur prend par defaut la couleur noire */

    if (joueur == 1) {
        case_joueur = NOIR;
        case_adversaire = BLANC;
    } else {
        case_joueur = BLANC;
        case_adversaire = NOIR;
    }
    /* on verifie que la case existe puis on verifie que les differents coups sont possibles  */
/* -----------------------------------------------------------------------------------------------  */
    /* on verifie que la case existe  */
    if (case_existe(ligne, colonne)==0 || m[ligne][colonne] != VIDE) return 0;   
    /* Coup vertical vers le haut */
    i = ligne - 1;
    valide = 0;
    while (case_existe(i, colonne)==1 && m[i][colonne] == case_adversaire) {
        i--;
        valide = 1;
    }
/* -----------------------------------------------------------------------------------------------  */
   /* on verifie que la case existe  */
    if (case_existe(i, colonne)==1 && m[i][colonne] == case_joueur && valide == 1) return 1;
    /* Coup vertical vers le bas */
    i = ligne + 1;
    valide = 0;
    while (case_existe(i, colonne)==1 && m[i][colonne] == case_adversaire) {
        i++;
        valide = 1;
    }
/* -----------------------------------------------------------------------------------------------  */
    /* on verifie que la case existe  */
    if (case_existe(i, colonne)==1 && m[i][colonne] == case_joueur && valide == 1) return 1;

    /* Coup horizontal vers la gauche */
    j = colonne- 1;
    valide = 0;
    while (case_existe(ligne, j)==1 && m[ligne][j] == case_adversaire) {
        j--;
        valide = 1;
    }
/* -----------------------------------------------------------------------------------------------  */
    /* on verifie que la case existe  */
    if (case_existe(ligne, j)==1 && m[ligne][j] == case_joueur && valide == 1) return 1;
    /* Coup horizontal vers la droite */
    j = colonne + 1;
    valide = 0;
    while (case_existe(ligne, j)==1 && m[ligne][j] == case_adversaire) {
        j++;
        valide = 1;
    }
/* -----------------------------------------------------------------------------------------------  */
    /* on verifie que la case existe  */
    if (case_existe(ligne, j)==1 && m[ligne][j] == case_joueur && valide == 1) return 1;
    /* Coup diagonal \ vers le haut */
    i = ligne - 1;
    j = colonne - 1;
    valide = 0;
    while (case_existe(i, j)==1 && m[i][j] == case_adversaire) {
        i--;
        j--;
        valide = 1;
    }
/* -----------------------------------------------------------------------------------------------  */
    /* on verifie que la case existe  */
    if (case_existe(i, j)==1 && m[i][j] == case_joueur && valide == 1) return 1;

    /* Coup diagonal \ vers le bas */
    i = ligne + 1;
    j = colonne + 1;
    valide = 0;
    while (case_existe(i, j)==1 && m[i][j] == case_adversaire) {
        i++;
        j++;
        valide = 1;
    }
/* -----------------------------------------------------------------------------------------------  */
    /* on verifie que la case existe  */
    if (case_existe(i, j)==1 && m[i][j] == case_joueur && valide == 1) return 1;

    /* Coup diagonal / vers le haut */
    i = ligne - 1;
    j = colonne + 1;
    valide = 0;
    while (case_existe(i, j)==1 && m[i][j] == case_adversaire) {
        i--;
        j++;
        valide = 1;
    }
/* -----------------------------------------------------------------------------------------------  */
    /* on verifie que la case existe  */
    if (case_existe(i, j)==1 && m[i][j] == case_joueur && valide == 1) return 1;

    /* Coup diagonal / vers le bas */
    i = ligne + 1;
    j = colonne- 1;
    valide = 0;
    while (case_existe(i, j)==1 && m[i][j] == case_adversaire) {
        i++;
        j--;
        valide = 1;
    }
/* -----------------------------------------------------------------------------------------------  */
    /* on verifie que la case existe  */
    if (case_existe(i, j)==1 && m[i][j] == case_joueur && valide == 1) return 1;

    return 0;
}

/* Fonction qui permet de jouer un coup */
void jouer_coup (grille m, int ligne, int colonne, int joueur) {
    int i, j;
    char case_joueur, case_adversaire;

    if (joueur == 1) {
        case_joueur = NOIR;
        case_adversaire = BLANC;
    } else {
        case_joueur = BLANC;
        case_adversaire = NOIR;
    }
    m[ligne][colonne] = case_joueur;

    /* Vertical vers le haut */
    i = ligne - 1;
    while (case_existe(i, colonne) && m[i][colonne] == case_adversaire) i--;
    if (case_existe(i, colonne) && m[i][colonne] == case_joueur) {
        i = ligne - 1;
        while (m[i][colonne] == case_adversaire) {
            m[i][colonne] = case_joueur;
            i--;
        }
    }

    /* Vertical vers le bas */
    i = ligne + 1;
    while (case_existe(i, colonne) && m[i][colonne] ==case_adversaire) i++;
    if (case_existe(i, colonne) && m[i][colonne] == case_joueur) {
        i = ligne + 1;
        while (m[i][colonne] == case_adversaire) {
            m[i][colonne] = case_joueur;
            i++;
        }
    }

    /* Horizontal vers la gauche */
    j = colonne - 1;
    while (case_existe(ligne, j) && m[ligne][j] == case_adversaire) j--;
    if (case_existe(ligne, j) && m[ligne][j] == case_joueur) {
        j = colonne - 1;
        while (m[ligne][j] == case_adversaire) {
            m[ligne][j] = case_joueur;
            j--;
        }
    }

    /* Horizontal vers la droite */
    j = colonne + 1;
    while (case_existe(ligne, j) && m[ligne][j] == case_adversaire) j++;
    if (case_existe(ligne, j) && m[ligne][j] == case_joueur) {
        j = colonne + 1;
        while (m[ligne][j] == case_adversaire) {
            m[ligne][j] = case_joueur;
            j++;
        }
    }

    /* Diagonal \ vers le haut */
    i = ligne - 1;
    j = colonne - 1;
    while (case_existe(i, j) && m[i][j] == case_adversaire) {
        i--;
        j--;
    }
    if (case_existe(i, j) && m[i][j] == case_joueur) {
        i = ligne - 1;
        j = colonne - 1;
        while (m[i][j] == case_adversaire) {
            m[i][j] = case_joueur;
            i--;
            j--;
        }
    }

    /* Diagonal \ vers le bas */
    i = ligne + 1;
    j = colonne + 1;
    while (case_existe(i, j) && m[i][j] == case_adversaire) {
        i++;
        j++;
    }
    if (case_existe(i, j) && m[i][j] == case_joueur) {
        i = ligne + 1;
        j = colonne + 1;
        while (m[i][j] == case_adversaire) {
            m[i][j] = case_joueur;
            i++;
            j++;
        }
    }

    /* Diagonal / vers le haut */
    i = ligne - 1;
    j = colonne + 1;
    while (case_existe(i, j) && m[i][j] == case_adversaire) {
        i--;
        j++;
    }
    if (case_existe(i, j) && m[i][j] == case_joueur) {
        i = ligne - 1;
        j = colonne + 1;
        while (m[i][j] == case_adversaire) {
            m[i][j] = case_joueur;
            i--;
            j++;
        }
    }

    /* Diagonal \ vers le bas */
    i = ligne + 1;
    j = colonne - 1;
    while (case_existe(i, j) && m[i][j] == case_adversaire) {
        i++;
        j--;
    }
    if (case_existe(i, j) && m[i][j] == case_joueur) {
        i = ligne + 1;
        j = colonne - 1;
        while (m[i][j] == case_adversaire) {
            m[i][j] = case_joueur;
            i++;
            j--;
        }
    }
}
/*passer la main au joueur suivant*/
int suivant(int joueur,char* j1,char* j2,char* j){
    if(strcmp(j,j1)==0)
       strcpy(j,j2);
    else
        strcpy(j,j1);
    if(joueur==1)
    {
        return 2;
    }
    else
    {
        return 1;
    }
}
/* Permet au joueur de choisir un coup */
void choisir_coup (grille m, int *ligne, int *colonne, int joueur) {
    char car;
    printf ("\nle tour du joueur n° %d \n", joueur);
    printf ("Choisissez une case : \n");
    scanf ("\n%c", &car);
    /* Si le joueur entre un caractere minuscule on change les minuscules en majuscules */
    if ((car >= 'a') && (car < 'a'+G))
        car= car + 'A' - 'a';
    (*colonne) = car - 'A';
    scanf ("%d", ligne);
    (*ligne)--;

    /* On redemande de choisir une autre case tant que le coup n'est pas accepte */
    while (coup_valide (m, *ligne, *colonne, joueur)!=1) {
        printf ("\n Le coup n'est pas valide\n");
        printf ("Choisissez une autre case (ex: C3 ou c3) :\n");
        scanf ("\n%c", &car);
        /* On change les minuscules en majuscules */
        if ((car >= 'a') && (car < 'a'+G))
            car = car + 'A' - 'a';
        (*colonne) = car - 'A';
        scanf ("%d", ligne);
        (*ligne)--;
    }
}


/*Compter le score des deux joueurs */
void score(grille m){

    int nb_B=0,nb_N=0;
    int i,j;
    for(i=0;i<8;i++){
        for(j=0;j<8;j++){
         /* Si la case est noire on incremente le nombre de 1 */
          if(m[i][j]==NOIR){ 
                nb_N++;}
        /* Si la case est blanche on incremente le nombre de 1 */
          else{
			if(m[i][j]==BLANC){
				nb_B++;}
        }
	}

}
printf(" Le score final est :  ");
printf(" %d pions noirs pour %d pions blancs\n",nb_N,nb_B);
}

/* Fonction qui determine si un joueur peut encore jouer */
int peut_jouer (grille m, int joueur) {
    int i, j;
    for (i=0; i<G; i++)
        for (j=0; j<G; j++)
            if (coup_valide(m, i, j, joueur)) return 1; /* Le joueur peut encore jouer */

    /* Le joueur ne peut plus jouer */
    return 0;
}

/* Verifie si la partie est terminee */
int partie_terminee (grille m) {
    int i, j, nb_noir, nb_blanc, cpt;
    nb_noir = 0;
    nb_blanc = 0;
    for (i=0; i<G; i++) {
        for (j=0; j<G; j++) {
           /* si une case est vide et un des deux joueurs peut encore jouer la partie n'est pas finie*/
            if (m[i][j] == VIDE && (peut_jouer(m, 1) || peut_jouer(m, 2)))
            {
                return 0; 
            } 
            /* si la partie est finie on calcule les pions noirs et blancs */
            else {
                if (m[i][j] == NOIR){ 
                    nb_noir++;
                }
                else if {
                    (m[i][j] == BLANC) nb_blanc++;
                }
            }
        }
    }

    /* La partie est terminee, on affiche le gagnant */
    if (nb_noir > nb_blanc){
        printf ("\nLe  gangnant est le joueur 1 \n");
    }
    else if (nb_blanc > nb_noir){
        printf ("\nLe  gangnant est le joueur 2\n");
    }
    else{
        printf ("\nLes deux joueurs sont a egalite\n");
    }

    /* On range les pions par couleur et on affiche la grille */
    cpt = 0;
    for (i=0; i<G; i++)
        for (j=0; j<G; j++) {
            if (cpt < nb_noir){
                m[i][j] = NOIR;
            }
            else if ((cpt >= nb_noir) && (cpt < nb_noir + nb_blanc -1)){
                m[i][j] = BLANC;
            }
            else { 
                m[i][j] = VIDE;
            }
            cpt++;
        }
    afficher_grille (m);
    printf ("\n");
    return 1;
}
/* Renvoie le numero du joueur suivant */
int joueur_suivant (int joueur) {
    return (joueur %2 + 1);
}
