#include "othello.h"

/* fonction qui crée et initialise tous les champs d’une structure coup_possible */
coup_possible* Cree_coup(int l,int c)
{
    coup_possible* coup=(coup_possible*)malloc(sizeof(coup_possible));
    coup->ligne=l;
    coup->colonne=c;
    coup->suiv=NULL;
    return coup;
}
/* fonction qui crée et initialise tous les champs d’une structure liste_coups */
liste_coups* Creeliste()
{
    liste_coups* l=(liste_coups*)malloc(sizeof(liste_coups));
    l->dernier=NULL;
    l->prem=NULL;
    l->nb_elem=0;
    return l;
}

/* fonction qui verifie si la liste des coups est vide */
int EstVide(liste_coups* l)
{
    return l->prem==NULL?1:0;
}

/* fonction qui ajoute une liste de coups */
void ajouter(liste_coups* l,int li,int c)
{
    coup_possible* coup=Cree_coup(li,c);
    if(EstVide(l))
    {
        l->prem=coup;
        l->dernier=coup;
    }
    else
    {
        coup->suiv=l->prem;
        l->prem=coup;
    }
    l->nb_elem++;
}

/* fonction qui retourne le nombre de coups valides et les ajoute a la grille*/
int coups(grille m,liste_coups* l,int joueur)
{
    int coups=0;
    int i,j;
    for(i=0;i<N;i++)
        {
            for(j=0;j<N;j++)
            {
                if(coup_valide(i,j,m,joueur))
                    {
                        ajouter(l,i,j);
                        coups++;
                    }
            }
        }
        return coups;
}

/* fonction qui copie les elements d'une grille source dans une autre grille destination */
void copier(char src[G][G],char dest[G][G])
{
    for(int i=0;i<G;i++)
    {
        for(int j=0;j<G;j++)
        {
            dest[i][j]=src[i][j];
        }
    }
}


int evaluation(grille m,int joueur)
{
    int nb_N=0,nb_B=0;
    int i,j;
    for(i=0;i<G;i++)
    {
        for(j=0;j<G;j++)
        {
            if(m[i][j]=='B')
                nb_B++;
            if(m[i][j]=='N')
                nb_N++;
        }
    }
    if(joueur==1)
        return nb_N-nb_B;
    return nb_B-nb_N;
}

void minimax(grille m,int profondeur,int joueur,int j,coup_possible* coup,int* score)
{
    coup_possible co;
    int S;
    int n;
    char temp[G][G];
    liste_coups* l=Creeliste();
    n=coups(m,l,joueur);
    if(n==0)
    {
        if(coup_possibles(m,3-joueur)==0)
        {
            *score=evaluation(m,j);
            return;
        }
    }
    if(joueur==j)
       *score=-Max;
    else
        *score=Max;
    profondeur--;
    coup_possible* e;
    e=l->prem;
    while(e!=NULL)
    {
        copier(m,temp);
        jouer_coup(e->ligne,e->colonne,temp,joueur);
        if(profondeur!=0)
            minimax(temp,profondeur,3-joueur,j,&co,&S);
        else
            S=evaluation(temp,j);
         if (joueur==j)
            { if (S >= *score) {
                *score = S;
                *coup = *e;
            }
            }
       else
       if(S <= *score){
            *score = S;
            *coup=*e;
       }
       e=e->suiv;
    }
}


void niveau_facile(grille m,int joueur,coup_possible* co)
{
    liste_coups* l=Creeliste();
    int n=coups(m,l,joueur);
    coup_possible* e=l->prem;
    char temp[G][G];
    int S=Max;
    while(e)
    {
        copier(m,temp);
        jouer_coup(e->ligne,e->colonne,temp,joueur);
        if(S >= evaluation(temp,joueur))
        {
            S=evaluation(temp,joueur);
            *co=*e;
        }
        e=e->suiv;
    }
}

void jvsmchine(int niveau_difficulte,int nouv_partie)
{
    int a=1;
    grille m;
    int l,ic;
    char c,q;
    int joueur=1,jou1=1,jou2=2;
	if(nouv_partie==1)
        {
            jou1=charger_joueur();
            jou2=3-jou1;
            if(jou1!=1)
                a=2;
            joueur=jou1;
            pos_init(m);
            charger_partie(m);
        }
    else
	char j1[9],j2[9],j[9];
	strcpy(j2,"machine");
	printf("entrer votre nom:\n");
	scanf("%s",j1);
	strcpy(j,j1);
    if(nouv_partie==0)
    {
        a=1+rand()%2;
        if(a==1)

            {
                printf("c'est %s qui va commencer\n",j1);
                strcpy(j,j1);
            }
        else
            {
                printf("c'est %s qui va commencer\n",j2);
                strcpy(j,j2);
                jou1=2;
                jou2=1;
            }
    }
    puts("appuyer sur une touche pour commencer:");
    getch();
    coup_possible co;
    int S,i=0;
    while(!fin_partie(m))
    {
        if(joueur==jou1)
        {
            do{
                    grille(m);
                    if(i!=0)
                        printf("\n\n\nla machine a joue:\n%d %c\n",co.ligne,co.colonne+65);
                    printf("\n A vous de jouer:%s\n",j1);
                    printf("appuyer sur une touche\n(q:quitter):");
                    q=(char)getchar();
                    if(q=='q' || q=='Q')///le joueur veut quitter la partie
                    {
                        enregistrer_partie(m);
                        enregister_joueur(jou1);
                        enregister_niveau(niveau_difficulte);

                        exit(NULL);
                    }
                    printf("donner la ligne:");
                    scanf("%d",&l);
                    printf("donner la colonne:");
                    scanf(" %c",&c);
                    if(c=='A' || c=='a')
                    else if(c=='B' || c=='b'){ic=1;}
                    else if(c=='C' || c=='c'){ic=2;}
                    else if(c=='D' || c=='d'){ic=3;}
                    else if(c=='E' || c=='e'){ic=4;}
                    else if(c=='F' || c=='f'){ic=5;}
                    else if(c=='G' || c=='g'){ic=6;}
                    else if(c=='H' || c=='h'){ic=7;}
            }while(!coup_valide(l,ic,m,joueur));
        jouer_coup(l,ic,m,joueur);
        }
        else{
            if(niveau_difficulte==0)
                niveau_facile(m,joueur,&co);
            else
                minimax(m,niveau_difficulte,joueur,joueur,&co,&S);
               jouer_coup(co.ligne,co.colonne,m,joueur);
            i++;
        }
        if(coup_possibles(m,suivant(joueur,j1,j2,j))!=0)
            joueur=suivant(joueur,j1,j2,j)
    grille(m);
    score(m);
    if(a==1)
    {
        if(gagnant(m)==1)
           {
               printf("Felicitations %s!",j1);
           }
        else if(gagnant(m)==2)
            {
                printf("Pas de chance %s a gagne!",j2);
            }
        else
            {
                printf("Egalite!");
            }
    }
    else{
        if(gagnant(m)==1)
            {
                printf("Pas de chance %s a gagne!",j2);
            }
        else if(gagnant(m)==2)
            {
                printf("Felicitations %s!",j1);
            }
        else
            {
                printf("Egalite!");
            }
  
}
int suivant(int joueur,char* j1,char* j2,char* j){
    if(strcmp(j,j1)==0)
       strcpy(j,j2);
    else
        strcpy(j,j1);
    if(joueur==1)
    {
        return 2;
    }
    else
    {
        return 1;
    }
}

