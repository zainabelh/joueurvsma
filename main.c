
#include "othello.h"

int main () {
    grille m;
	int ligne, colonne, joueur = 1;
    
    /* Initialisation du jeu */
    init_grille (m);
    afficher_grille (m);
	/* Deroulement d'une partie */
    while (!partie_terminee (m)) {
        choisir_coup (m, &ligne, &colonne, joueur);
        jouer_coup (m, ligne, colonne, joueur);
        afficher_grille(m);
        if (peut_jouer(m, joueur_suivant(joueur)))
            joueur = joueur_suivant (joueur);
        else printf ("\nLe joueur %d passe son tour\n", joueur_suivant(joueur));
    }
	score(m);
    return 0;
}
